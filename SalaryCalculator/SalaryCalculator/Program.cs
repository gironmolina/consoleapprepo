﻿using System;
using System.Collections.Generic;
using TakeHomeCalculator.Core;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator
{
    internal class Program
    {
        private static bool _isHoursWorkedValidated, _isHourlyRateValidated, _isLocationValidated;
        private static string _hoursWorked, _hourlyRate, _location;
        private static ITaxes _taxes;

        private static void Main()
        {
            var countries = new Dictionary<int, string>
            {
                {1, "Ireland"},
                {2, "Italy"},
                {3, "Germany"},
                {4, "N/A"}
            };

            Console.WriteLine("Please enter the hours worked:"); //TODO validar double
            do
            {
                _hoursWorked = Console.ReadLine();
                _isHoursWorkedValidated = Utils.ValidateDoubleValue(_hoursWorked);
            } while (!_isHoursWorkedValidated);
            
            Console.WriteLine("Please enter the hourly rate:");
            do
            {
                _hourlyRate = Console.ReadLine();
                _isHourlyRateValidated = Utils.ValidateDoubleValue(_hourlyRate);
            } while (!_isHourlyRateValidated);
            
            Console.WriteLine("Please enter the employees location:");
            foreach (var country in countries)
            {
                Console.WriteLine("{0} - {1}", country.Key, country.Value);
            }

            do
            {
                _location = Console.ReadLine();
                _isLocationValidated = Utils.ValidateIntValue(_location, 1, 4);
            } while (!_isLocationValidated);

            switch (Convert.ToInt32(_location))
            {
                case 1:
                    _taxes = CountryTaxFactory.BuildInstance(CountryTaxFactory.CountryEnum.Ireland);
                    Console.WriteLine(" The gross amount is: {0}", _taxes.CalculateTaxes(Convert.ToDouble(_hoursWorked), Convert.ToDouble(_hourlyRate)));
                    break;
                case 2:
                    _taxes = CountryTaxFactory.BuildInstance(CountryTaxFactory.CountryEnum.Italy);
                    Console.WriteLine(" The gross amount is: {0}", _taxes.CalculateTaxes(Convert.ToDouble(_hoursWorked), Convert.ToDouble(_hourlyRate)));
                    break;
                case 3:
                    _taxes = CountryTaxFactory.BuildInstance(CountryTaxFactory.CountryEnum.Germany);
                    Console.WriteLine(" The gross amount is: {0}", _taxes.CalculateTaxes(Convert.ToDouble(_hoursWorked), Convert.ToDouble(_hourlyRate)));
                    break;
                case 4:
                    _taxes = CountryTaxFactory.BuildInstance(CountryTaxFactory.CountryEnum.Default);
                    Console.WriteLine(" The gross amount is: {0}", _taxes.CalculateTaxes(Convert.ToDouble(_hoursWorked), Convert.ToDouble(_hourlyRate)));
                    break;
            }
            Console.ReadKey();
        }
    }
}
