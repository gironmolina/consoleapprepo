﻿namespace TakeHomeCalculator.Interfaces
{
    public interface ITaxes
    {
        double Tax { get; set; }
        double UniversalSocialCharge { get; set; }
        double Pension { get; set; }

        double CalculateTaxes(double hoursWorked, double hourlyRate);
    }
}
