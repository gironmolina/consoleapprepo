﻿using System;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator.Countries
{
    public class GermanyTax : ITaxes
    {
        public double Tax { get; set; }
        public double UniversalSocialCharge { get; set; }
        public double Pension { get; set; }

        public GermanyTax()
        {
            Pension = 2;
        }

        public double CalculateTaxes(double hoursWorked, double hourlyRate)
        {
            var gross = hoursWorked * hourlyRate;
            Tax = gross < 400 ? 25 : 32;
            var taxToPay = gross * (Tax / 100);
            var pensionToPay = gross * (Pension / 100);

            return Math.Round(gross - taxToPay - pensionToPay, 1);
        }
    }
}
