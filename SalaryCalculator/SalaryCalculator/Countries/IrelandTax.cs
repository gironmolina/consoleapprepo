﻿using System;
using System.Text.RegularExpressions;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator.Countries
{
    public class IrelandTax : ITaxes
    {
        public double Tax { get; set; }
        public double UniversalSocialCharge { get; set; }
        public double Pension { get; set; }

        public IrelandTax()
        {
            Pension = 4;
        }

        public double CalculateTaxes(double hoursWorked, double hourlyRate)
        {
            var gross = hoursWorked * hourlyRate;
            Tax = gross < 600 ? 25 : 40;
            UniversalSocialCharge = gross < 400 ? 7 : 8;
            var taxToPay = gross * (Tax/100);
            var universalSocialChargeToPay = gross * (UniversalSocialCharge/100);
            var pensionToPay = gross * (Pension / 100);
            return Math.Round(gross - taxToPay - universalSocialChargeToPay - pensionToPay, 1);
        }

    }
}
