﻿using System;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator.Countries
{
    public class ItalyTax : ITaxes
    {
        public double Tax { get; set; }
        public double UniversalSocialCharge { get; set; }
        public double Pension { get; set; }

        public ItalyTax()
        {
            Tax = 25;
            Pension = 9.19;
        }

        public double CalculateTaxes(double hoursWorked, double hourlyRate)
        {
            var gross = hoursWorked * hourlyRate;
            var taxToPay = gross * (Tax / 100);
            var pensionToPay = gross * (Pension / 100);

            return Math.Round(gross - taxToPay - pensionToPay, 1);
        }
    }
}
