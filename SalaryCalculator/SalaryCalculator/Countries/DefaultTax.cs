﻿using System;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator.Countries
{
    public class DefaultTax : ITaxes
    {
        public double Tax { get; set; }
        public double UniversalSocialCharge { get; set; }
        public double Pension { get; set; }

        public double CalculateTaxes(double hoursWorked, double hourlyRate)
        {
            return Math.Round(hoursWorked*hourlyRate, 1);
        }
    }
}
