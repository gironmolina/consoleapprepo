﻿using System;
using TakeHomeCalculator.Countries;
using TakeHomeCalculator.Interfaces;

namespace TakeHomeCalculator.Core
{
    public class CountryTaxFactory
    {
        public static ITaxes BuildInstance(CountryEnum type)
        {
            switch (type)
            {
                case CountryEnum.Ireland:
                    return new IrelandTax();

                case CountryEnum.Italy:
                    return new ItalyTax();

                case CountryEnum.Germany:
                    return new GermanyTax();

                case CountryEnum.Default:
                    return new DefaultTax();

                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }
        }

        public enum CountryEnum
        {
            Ireland,
            Italy,
            Germany,
            Default
        }
    }
}
