﻿using System;

namespace TakeHomeCalculator.Core
{
    public static class Utils
    {
        public static bool ValidateIntValue(string currentValue, int minRange = 0, int maxRange = 999)
        {
            int number;
            if (int.TryParse(currentValue, out number) && IsWithin(Convert.ToInt32(currentValue), minRange, maxRange) && minRange < maxRange)
            {
                return true;
            }

            Console.WriteLine("Not a valid number, try again.");
            return false;
        }

        public static bool ValidateDoubleValue(string currentValue, int minRange = 0, int maxRange = 999)
        {
            double number;
            if (double.TryParse(currentValue, out number) && IsWithin(Convert.ToInt32(currentValue), minRange, maxRange) && minRange < maxRange)
            {
                return true;
            }

            Console.WriteLine("Not a valid number, try again.");
            return false;
        }

        public static bool IsWithin(double value, double minimum, double maximum)
        {
            return value >= minimum && value <= maximum;
        }
    }
}
