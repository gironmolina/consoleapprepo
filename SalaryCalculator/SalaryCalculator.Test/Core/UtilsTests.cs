﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TakeHomeCalculator.Core;

namespace TakeHomeCalculatorTest.Core
{
    [TestClass]
    public class UtilsTests
    {
        [TestMethod]
        public void ValidateIntValueTest()
        {
            var currentValue = "8";
            var minRange = 0;
            var maxRange = 10;
            var actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreEqual(true, actual);

            currentValue = "asd";
            minRange = 4;
            maxRange = 6;
            actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreEqual(false, actual);

            currentValue = "12";
            minRange = 15;
            maxRange = 10;
            actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void ValidateDoubleValueTest()
        {
            var currentValue = "8";
            var minRange = 0;
            var maxRange = 10;
            var actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreEqual(true, actual);

            currentValue = "asd";
            minRange = 4;
            maxRange = 6;
            actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreEqual(false, actual);

            currentValue = "12,2";
            minRange = 15;
            maxRange = 10;
            actual = Utils.ValidateIntValue(currentValue, minRange, maxRange);
            Assert.AreNotEqual(true, actual);
        }

        [TestMethod]
        public void IsWithinTest()
        {
            double currentValue = 2;
            var minRange = 0;
            var maxRange = 10;
            var actual = Utils.IsWithin(currentValue, minRange, maxRange);
            Assert.AreEqual(true, actual);

            currentValue = 12.2;
            minRange = 15;
            maxRange = 10;
            actual = Utils.IsWithin(currentValue, minRange, maxRange);
            Assert.AreEqual(false, actual);
        }
    }
}
