﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TakeHomeCalculator.Countries;

namespace TakeHomeCalculatorTest.Countries
{
    [TestClass]
    public class DefaultTaxTests
    {
        [TestMethod]
        public void CalculateTaxesTest()
        {
            var target = new DefaultTax(); 
            var hoursWorked = 8;
            var hourlyRate = 12;
            var actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(96, actual);

            hoursWorked = 2;
            hourlyRate = 20;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreNotEqual(96, actual);
        }
    }
}
