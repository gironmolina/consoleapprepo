﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TakeHomeCalculator.Countries;

namespace TakeHomeCalculatorTest.Countries
{
    [TestClass]
    public class GermanyTaxTests
    {
        [TestMethod]
        public void CalculateTaxesTest()
        {
            var target = new GermanyTax();
            var hoursWorked = 4;
            var hourlyRate = 7;
            var actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(20.4, actual);

            hoursWorked = 10;
            hourlyRate = 40;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(264, actual);

            hoursWorked = 220;
            hourlyRate = 40;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreNotEqual(123, actual);
        }
    }
}
