﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TakeHomeCalculator.Countries;

namespace TakeHomeCalculatorTest.Countries
{
    [TestClass]
    public class ItalyTaxTests
    {
        [TestMethod]
        public void CalculateTaxesTest()
        {
            var target = new ItalyTax();
            var hoursWorked = 2;
            var hourlyRate = 801;
            var actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(1054.3, actual);

            hoursWorked = 52;
            hourlyRate = 110;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(3764.3, actual);

            hoursWorked = 3;
            hourlyRate = 333;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreNotEqual(432, actual);
        }
    }
}
