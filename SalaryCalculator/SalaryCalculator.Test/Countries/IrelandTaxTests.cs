﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TakeHomeCalculator.Countries;

namespace TakeHomeCalculatorTest.Countries
{
    [TestClass]
    public class IrelandTaxTests
    {
        [TestMethod]
        public void CalculateTaxesTest()
        {
            var target = new IrelandTax();
            var hoursWorked = 4;
            var hourlyRate = 100;
            var actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(252, actual);

            hoursWorked = 2;
            hourlyRate = 123;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreEqual(157.4, actual);

            hoursWorked = 5;
            hourlyRate = 180;
            actual = target.CalculateTaxes(hoursWorked, hourlyRate);
            Assert.AreNotEqual(999, actual);
        }
    }
}
