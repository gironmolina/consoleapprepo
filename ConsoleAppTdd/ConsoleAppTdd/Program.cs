﻿using System;
using System.Linq;

namespace ConsoleAppTdd
{
    public class Program
    {
        public static void Main()
        {
            Console.Write("Enter upper limit: ");
            var readLine = Console.ReadLine();
            if (readLine == null)
                return;

            var range = int.Parse(readLine);

            for (var i = 0; i < range; i++)
            {
                var num = i % 7 == 0 || i.ToString().Contains('7') ? "BOOM" : i.ToString();
                Console.Write("{0}, ", num);
            }
        }
    }
}
